# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.6.10] - 2022-02-02

### Added
- semantic version support in python poetry project
- release start <version>
- release submit
- release finish
- publish start
- python version bump
- python version get

### Changed

### Deprecated

### Removed

### Fixed

### Security


## [0.4.2] - 2022-01-27

### Added
- general feature support (start, add-commit, finish)
- apple build and test support

### Changed

### Deprecated

### Removed

### Fixed

### Security
