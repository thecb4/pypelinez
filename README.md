# Pypelines

## Description
Python command line tool to support CI/CD for various platforms

[![pipeline status](https://gitlab.com/thecb4/pypelinez/badges/main/pipeline.svg)](https://gitlab.com/thecb4/pypelinez/-/commits/main)

-- images --

## Installation
pip install pypelinez

## Usage
```shell
$ pypelinez feature start <feature/feature-name>
```

Current commands:
* feature start
* feature add-commit
* feature submit
* feature finish
* release start <version>
* release add-commit
* release submit
* release finish
* release publish-start

## Support

## Roadmap
[ ] apple version bump
[ ] android version bump
[ ] documentation
[ ] Simplify version bump process

## Contributing

## Authors and Acknowledgements

## License
[MIT](https://choosealicense.com/licenses/mit/)