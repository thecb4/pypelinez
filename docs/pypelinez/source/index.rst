.. pypelinez documentation master file, created by
   sphinx-quickstart on Thu Feb  3 17:12:28 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to |ProjectVersion| documentation!
==========================================

.. click:: pypelinez.main:main
   :prog: pypelinez
   :nested: full

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
